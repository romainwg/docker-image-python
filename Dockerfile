FROM python:3.10-alpine

# Set workdir https://docs.docker.com/engine/reference/builder/#workdir
WORKDIR /app

# Copy all to workdir
COPY requirements.txt .

# Install python package
RUN pip install -r requirements.txt

# Add zip to image
RUN apk add zip

